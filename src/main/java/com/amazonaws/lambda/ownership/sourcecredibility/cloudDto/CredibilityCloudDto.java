package com.amazonaws.lambda.ownership.sourcecredibility.cloudDto;

import java.io.Serializable;

import com.amazonaws.lambda.ownership.sourcecredibility.dto.DataAttributesDto;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.SubClassificationsDto;
import com.amazonaws.lambda.ownership.sourcecredibility.enumType.CredibilityEnums;

public class CredibilityCloudDto implements Serializable {

	private Long sourceCredibilityId;

	private Long sourceId;

	private SubClassificationsDto subClassifications;

	private DataAttributesDto dataAttributes;

	private CredibilityEnums credibility;

	public Long getSourceCredibilityId() {
		return sourceCredibilityId;
	}

	public void setSourceCredibilityId(Long sourceCredibilityId) {
		this.sourceCredibilityId = sourceCredibilityId;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public SubClassificationsDto getSubClassifications() {
		return subClassifications;
	}

	public void setSubClassifications(SubClassificationsDto subClassifications) {
		this.subClassifications = subClassifications;
	}

	public DataAttributesDto getDataAttributes() {
		return dataAttributes;
	}

	public void setDataAttributes(DataAttributesDto dataAttributes) {
		this.dataAttributes = dataAttributes;
	}

	public CredibilityEnums getCredibility() {
		return credibility;
	}

	public void setCredibility(CredibilityEnums credibility) {
		this.credibility = credibility;
	}
}
