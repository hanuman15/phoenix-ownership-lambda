package com.amazonaws.lambda.ownership.sourcecredibility.cloudservice;

import java.util.List;

import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourceIndustryDto;

public interface IndustryCloudSerive {
	
	public List<SourceIndustryDto> getInsdustrySource(String type);

}
