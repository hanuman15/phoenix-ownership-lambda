package com.amazonaws.lambda.ownership.sourcecredibility.cloudDto;

import java.util.List;

import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourcesDto;

public class SourcesPaginationDto {
	List<SourcesDto> sourceList;
	Long totalRecord;
	public List<SourcesDto> getSourceList() {
		return sourceList;
	}
	public void setSourceList(List<SourcesDto> sourceList) {
		this.sourceList = sourceList;
	}
	public Long getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(Long totalRecord) {
		this.totalRecord = totalRecord;
	}
	
	

}
