package com.amazonaws.lambda.ownership.sourcecredibility.cloudservice;

import java.util.List;

import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.DataAttributesCloudDto;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.SaveDataAttributeCloudDto;

public interface DataAttributesCloudService {
	
	public List <DataAttributesCloudDto> getDatAttributes();
	
	public Boolean deleteDataAttributesById(Long attrId);
	
	public Boolean saveDataAttributes(SaveDataAttributeCloudDto saveCloudDto);
	
	public Boolean updateDataAttributes( SaveDataAttributeCloudDto dataAttributeDto);

}
