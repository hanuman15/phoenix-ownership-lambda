package com.amazonaws.lambda.ownership.sourcecredibility.cloudservice;

import java.util.List;

import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.SourcesCloudDto;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.SourcesPaginationDto;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourceFilterDto;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourcesDto;

public interface SourceCloudService {
	
	public SourcesPaginationDto getSources( Integer pageNumber, Integer recordsPerPage, Boolean visible,
			Long classificationId, String orderBy, String orderIn, List<SourceFilterDto> sourceFilterDto,
			Long subClassifcationId, Boolean isAllSourcesRequired);
	
	public Boolean saveSource(SourcesDto sourcesDto,Long userId);
	
	public Boolean updateSource(SourcesDto sourcesDto,Long userId);
	
	public SourcesCloudDto getSourceById(Long sourceId);
	
}
