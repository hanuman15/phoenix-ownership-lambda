package com.amazonaws.lambda.ownership.sourcecredibility.cloudserviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.amazonaws.lambda.ownership.rest.ServiceCallHelper;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.CredibilityCloudDto;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudservice.CredibilityCloudService;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.DataAttributesDto;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.SubClassificationsDto;
import com.amazonaws.lambda.ownership.sourcecredibility.enumType.CredibilityEnums;
@Service
//@PropertySource(value = "classpath:serviceurls.properties")
public class CredibilityCloudServiceImpl implements CredibilityCloudService{
	@Autowired
	 Environment env;
	ResourceBundle resource = ResourceBundle.getBundle("application");
	//@Value("${source_management_cloud_url}")
	private String SOURCE_MANAGEMENT_CLOUD_URL=System.getenv("SOURCE_MANAGEMENT_CLOUD_URL");
	
	private static String genericEntity="credibilities";
	
	//@Value("${client_id}")
	private String clientId=System.getenv("CLIENT_ID");

	@Override
	public List<CredibilityCloudDto> getCredibility() {
		List<CredibilityCloudDto> credibilityList = new ArrayList<>();
	
		String url = buildCloudUrl(null);
	//	JSONObject json = new JSONObject();
		JSONObject mainJson = new JSONObject();
		try {
			String response = getCloudApiData(url);
			if (response != null) {
				mainJson = new JSONObject(response);
				if (mainJson.has("data_link") && mainJson.has("status") && mainJson.getBoolean("status")) {
					String dataLink = mainJson.getString("data_link");
					byte[] bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeout(dataLink);
					if (bytes != null) {
						String linkData = new String(bytes);
						JSONObject json = new JSONObject(linkData);
						JSONArray data = json.getJSONArray("data");
						for (int i = 0; i < data.length(); i++) {
							CredibilityCloudDto singleDto = new CredibilityCloudDto();
							JSONObject singleJson = new JSONObject();
							singleJson = data.getJSONObject(i);
							if (singleJson.has("credibility_id"))
								singleDto.setSourceCredibilityId(singleJson.getLong("credibility_id"));

							if (singleJson.has("source_id"))
								singleDto.setSourceId(singleJson.getLong("source_id"));

							if (singleJson.has("data_attribute")
									&& singleJson.get("data_attribute") instanceof JSONObject
									&& singleJson.get("data_attribute") != null) {
								DataAttributesDto attributedto = new DataAttributesDto();
								if (singleJson.get("data_attribute") instanceof JSONObject
										&& singleJson.getJSONObject("data_attribute").has("attribute_id"))
									attributedto.setAttributeId(
											singleJson.getJSONObject("data_attribute").getLong("attribute_id"));

								if (singleJson.getJSONObject("data_attribute").has("name"))
									attributedto.setSourceAttributeName(
											singleJson.getJSONObject("data_attribute").getString("name"));

								if (singleJson.getJSONObject("data_attribute").has("schema"))
									attributedto.setSourceAttributeSchema(
											singleJson.getJSONObject("data_attribute").getString("schema"));

								if (singleJson.getJSONObject("data_attribute").has("schema_group"))
									attributedto.setSourceAttributeSchemaGroup(
											singleJson.getJSONObject("data_attribute").getString("schema_group"));
								if (singleJson.has("credibility_code_id")) {
									int credibilityenum = singleJson.getInt("credibility_code_id");
									if (credibilityenum == 0)
										attributedto.setCredibilityValue(CredibilityEnums.NONE);
									if (credibilityenum == 1)
										attributedto.setCredibilityValue(CredibilityEnums.LOW);
									if (credibilityenum == 2)
										attributedto.setCredibilityValue(CredibilityEnums.MEDIUM);
									if (credibilityenum == 3)
										attributedto.setCredibilityValue(CredibilityEnums.HIGH);
								}
								singleDto.setDataAttributes(attributedto);
							}

							if (singleJson.has("sub_classification") && singleJson.get("sub_classification") instanceof JSONObject) {
								SubClassificationsDto subClassDto = new SubClassificationsDto();
								if (singleJson.getJSONObject("sub_classification").has("name"))
									subClassDto.setSubClassifcationName(
											singleJson.getJSONObject("sub_classification").getString("name"));
								if (singleJson.getJSONObject("sub_classification").has("item_id"))
									subClassDto.setSubClassificationId(
											singleJson.getJSONObject("sub_classification").getLong("item_id"));
								if (singleJson.getJSONObject("sub_classification").has("name"))
									subClassDto.setSubClassifcationName(
											singleJson.getJSONObject("sub_classification").getString("name"));
								if (singleJson.has("credibility_code_id")) {
									int credibilityenum = singleJson.getInt("credibility_code_id");
									if (credibilityenum == 0)
										subClassDto.setSubClassificationCredibility(CredibilityEnums.NONE);
									if (credibilityenum == 1)
										subClassDto.setSubClassificationCredibility(CredibilityEnums.LOW);
									if (credibilityenum == 2)
										subClassDto.setSubClassificationCredibility(CredibilityEnums.MEDIUM);
									if (credibilityenum == 3)
										subClassDto.setSubClassificationCredibility(CredibilityEnums.HIGH);
								}
								// Add DataAttribute for subClassification.
								singleDto.setSubClassifications(subClassDto);
							}
							if (singleJson.has("credibility_code_id")) {
								int credibilityenum = singleJson.getInt("credibility_code_id");
								if (credibilityenum == 0)
									singleDto.setCredibility(CredibilityEnums.NONE);
								if (credibilityenum == 1)
									singleDto.setCredibility(CredibilityEnums.LOW);
								if (credibilityenum == 2)
									singleDto.setCredibility(CredibilityEnums.MEDIUM);
								if (credibilityenum == 3)
									singleDto.setCredibility(CredibilityEnums.HIGH);
							}
							credibilityList.add(singleDto);
						}
					}

				}
			}else {
				throw new RuntimeException("Not Data Received");
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		}

		return credibilityList;
	}
	
	@Override
	public Boolean saveCredibility(CredibilityCloudDto credibilityDto) {
		JSONObject jsonToSent = new JSONObject();
		if (credibilityDto.getCredibility() == CredibilityEnums.LOW)
			jsonToSent.put("credibility_code_id", 1);
		if (credibilityDto.getCredibility() == CredibilityEnums.HIGH)
			jsonToSent.put("credibility_code_id", 3);
		if (credibilityDto.getCredibility() == CredibilityEnums.MEDIUM)
			jsonToSent.put("credibility_code_id", 2);
		if (credibilityDto.getCredibility() == CredibilityEnums.NONE)
			jsonToSent.put("credibility_code_id", 0);
		jsonToSent.put("source_id", credibilityDto.getSourceId());
		jsonToSent.put("sub_classification_id", credibilityDto.getSubClassifications().getSubClassificationId());
		if(credibilityDto.getDataAttributes().getAttributeId()!=null && credibilityDto.getDataAttributes().getAttributeId()>0l)
			jsonToSent.put("data_attribute_id", credibilityDto.getDataAttributes().getAttributeId());
		jsonToSent.put("client_id", clientId);
		Boolean saved = null;
		String url = buildCloudUrl(null);
		try {
			String response = postCloudApiData(url, jsonToSent.toString());
			if (response != null) {
				JSONObject responseJson = new JSONObject(response);
				if (responseJson.has("credibility_id") && responseJson.has("message")
						&& responseJson.getString("message").equals("saved")) {
					saved = true;
					return saved;
				} else {
					saved = false;
					return saved;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return saved;
	}
	
	@Override
	public Boolean updateCredibility(CredibilityCloudDto credibilityDto) {
		JSONObject jsonToSent = new JSONObject();
		if(credibilityDto.getCredibility()==CredibilityEnums.LOW)
			jsonToSent.put("credibility_code_id",1);
		if(credibilityDto.getCredibility()==CredibilityEnums.HIGH)
			jsonToSent.put("credibility_code_id",3);
		if(credibilityDto.getCredibility()==CredibilityEnums.MEDIUM)
			jsonToSent.put("credibility_code_id",2);
		if(credibilityDto.getCredibility()==CredibilityEnums.NONE)
			jsonToSent.put("credibility_code_id",0);
		jsonToSent.put("source_id",credibilityDto.getSourceId());
		jsonToSent.put("sub_classification_id",credibilityDto.getSubClassifications().getSubClassificationId());
		jsonToSent.put("credibility_id", credibilityDto.getSourceCredibilityId());
		if(credibilityDto.getDataAttributes().getAttributeId()!=null && credibilityDto.getDataAttributes().getAttributeId()>0l)
			jsonToSent.put("data_attribute_id", credibilityDto.getDataAttributes().getAttributeId());
		jsonToSent.put("client_id", clientId);
		Boolean saved = null;
		String url = buildCloudUrl(null);
		try {
			String response = putCloudApiData(url, jsonToSent.toString());
			if (response != null) {
				JSONObject responseJson = new JSONObject(response);
				if (responseJson.has("credibility_id") && responseJson.has("message")
						&& responseJson.getString("message").equals("updated")) {
					saved = true;
					return saved;
				} else {
					saved = false;
					return saved;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return saved;
	}
	
	@Override
	public Boolean deleteCredibilityById(Long id) {
		String url = buildCloudUrl(id);
		Boolean deleteResult = null;
		try {
			String getresponse = deleteCloudApiData(url);
			if (getresponse != null) {
				JSONObject getResponseJson = new JSONObject(getresponse);
				if (getResponseJson.has("message") && getResponseJson.getString("message").equals("deleted")) {
					deleteResult = true;
					return deleteResult;
				} else
					deleteResult = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return deleteResult;
	}
	public String getCloudApiData(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("data")) {
					return response;
				}
			} else {
				return response;
			}
			return response;
		}
		return response;
	}
	
	
	
	public String deleteCloudApiData(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.deleteDataInServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("message"))
					return response;
			} else {
				return response;
			}
			return response;
		}
		return response;
	}
	
	public String putCloudApiData(String url, String jsonToSent) throws Exception {
		String response = null;
		String serverResponse[] =ServiceCallHelper.putStringDataToServerWithoutTimeout(url, jsonToSent);
		
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("data_attribute_id")) {
					return response;
				}
			} else {
				return response;
			}
			return response;
		}
		return response;
	}
	
	public String postCloudApiData(String url, String jsonToSent) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.postStringDataToServer(url, jsonToSent);
		
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("data_attribute_id")) {
					return response;
				}
			} else {
				return response;
			}
			return response;
		}
		return response;
	}
	
	public String buildCloudUrl(Long id) {
		if (id!=null && id>=0l ) {
			String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"?credibility_id="+id;
			return url;
		}else if(id!=null && id<0l){
			String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"?recordsPerPage=500&pageNumber=31";
			return url;
		}
		else {
			String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity;
			return url;
		}
	}
}
