package com.amazonaws.lambda.ownership.sourcecredibility.cloudservice;

import java.util.List;

import com.amazonaws.lambda.ownership.sourcecredibility.dto.ClassificationsDto;

public interface ClassificationCloudService {
	
	public List<ClassificationsDto> getClassifications(String type);

}
