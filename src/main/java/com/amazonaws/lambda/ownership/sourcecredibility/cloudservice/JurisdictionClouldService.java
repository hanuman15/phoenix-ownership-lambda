package com.amazonaws.lambda.ownership.sourcecredibility.cloudservice;

import java.util.List;

import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourceJurisdictionDto;

public interface JurisdictionClouldService {
 
	public List<SourceJurisdictionDto> getSourceJurisdiction();
}
