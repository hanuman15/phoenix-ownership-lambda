package com.amazonaws.lambda.ownership.sourcecredibility.cloudservice;

import java.util.List;

import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.GenericEntityDto;

public interface GenericCloudService {
	
	public GenericEntityDto saveGenericEntity(GenericEntityDto genericEntityDto);

	public GenericEntityDto getGenericItemById(Long id);
	
	public List<GenericEntityDto> getAllGenericItem();
	
	public List<GenericEntityDto> getGenericItemByType(String type);
	
	public Boolean deleteGenericEntityByid(Long id);
	
	public GenericEntityDto updateGenericEntity(GenericEntityDto genericEntityDto);
}
