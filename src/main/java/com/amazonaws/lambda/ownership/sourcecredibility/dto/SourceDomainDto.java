package com.amazonaws.lambda.ownership.sourcecredibility.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class SourceDomainDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long domainId;

	private String domainName;

	public SourceDomainDto() {
	}

	public SourceDomainDto(String domainName) {
		super();
		this.domainName = domainName;
	}

	public Long getDomainId() {
		return domainId;
	}

	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

}
