package com.amazonaws.lambda.ownership.sourcecredibility.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class SourceMediaDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long mediaId;

	private String mediaName;

	public SourceMediaDto() {
	}

	public SourceMediaDto(String mediaName) {
		super();
		this.mediaName = mediaName;
	}

	public Long getMediaId() {
		return mediaId;
	}

	public void setMediaId(Long mediaId) {
		this.mediaId = mediaId;
	}

	public String getMediaName() {
		return mediaName;
	}

	public void setMediaName(String mediaName) {
		this.mediaName = mediaName;
	}

}
