package com.amazonaws.lambda.ownership.sourcecredibility.enumType;

/**
 * 
 * @author Suresh
 *
 */
public enum CredibilityEnums {

	NONE, LOW, MEDIUM, HIGH

}
