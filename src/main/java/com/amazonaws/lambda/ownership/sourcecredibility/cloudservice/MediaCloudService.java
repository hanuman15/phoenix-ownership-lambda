package com.amazonaws.lambda.ownership.sourcecredibility.cloudservice;

import java.util.List;

import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourceMediaDto;

public interface MediaCloudService {
	
	public List<SourceMediaDto> getSourceMedia(String type);
	
}
