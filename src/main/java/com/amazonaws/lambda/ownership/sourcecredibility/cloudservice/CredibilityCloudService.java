package com.amazonaws.lambda.ownership.sourcecredibility.cloudservice;

import java.util.List;

import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.CredibilityCloudDto;

public interface CredibilityCloudService {

	public List<CredibilityCloudDto> getCredibility();

	public Boolean saveCredibility(CredibilityCloudDto credibilityDto);
	
	public Boolean updateCredibility(CredibilityCloudDto credibilityDto);
	
	public Boolean deleteCredibilityById(Long id);
}
