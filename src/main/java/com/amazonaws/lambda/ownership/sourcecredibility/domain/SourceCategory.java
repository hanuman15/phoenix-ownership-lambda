package com.amazonaws.lambda.ownership.sourcecredibility.domain;

import java.io.Serializable;
/*
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;*/

/*
 * @author Prateek Maurya
 */
/*@Entity
@Table(name = "sc_category")*/
public class SourceCategory implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy = GenerationType.AUTO)
	 */
	private Long categoryId;

	/* @Column(name = "categoryName") */
	private String categoryName;

	public SourceCategory() {

	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}
