package com.amazonaws.lambda.ownership.sourcecredibility.cloudservice;

import java.util.List;

import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourceDomainDto;

public interface DomainCloudService {

	public List<SourceDomainDto> getSourceDomain(String type);
}
