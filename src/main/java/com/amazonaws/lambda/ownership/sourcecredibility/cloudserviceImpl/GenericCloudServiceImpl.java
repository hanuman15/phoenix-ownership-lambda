package com.amazonaws.lambda.ownership.sourcecredibility.cloudserviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.amazonaws.lambda.ownership.rest.ServiceCallHelper;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.GenericEntityDto;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.GenericResponseDto;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudservice.GenericCloudService;

@Service
//@PropertySource(value = "classpath:serviceurls.properties")
public class GenericCloudServiceImpl implements GenericCloudService {
	
	@Autowired
	 Environment env;
	ResourceBundle resource = ResourceBundle.getBundle("application");
	//@Value("${source_management_cloud_url}")
	private String SOURCE_MANAGEMENT_CLOUD_URL=System.getenv("SOURCE_MANAGEMENT_CLOUD_URL");
	
	private static String genericEntity="listitems";
	
	private String clientId=System.getenv("CLIENT_ID");

	@Override
	public GenericEntityDto saveGenericEntity(GenericEntityDto genericEntityDto) {
		String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity;
		GenericResponseDto respDto= new GenericResponseDto();
		GenericEntityDto getEntityDto=new GenericEntityDto();
		JSONObject json= new JSONObject();
		if(genericEntityDto!=null) {
			if(genericEntityDto.getName()!=null)
				json.put("name", genericEntityDto.getName());
			json.put("client_id", clientId);
			if(genericEntityDto.getParentId()!=null)
				json.put("parent_id", genericEntityDto.getParentId());
			if(genericEntityDto.getType()!=null)
				json.put("type", genericEntityDto.getType());
			
			if(genericEntityDto.getSubItems()!=null) {
				JSONArray subListArray= new JSONArray();
				for(GenericEntityDto sub:genericEntityDto.getSubItems()) {
					JSONObject sublist= new JSONObject();
					sublist.put("client_id", clientId);
					if (sub.getCode() != null)
						sublist.put("code", sub.getCode());
					if(sub.getColor()!=null)
						sublist.put("color", sub.getColor());
					if(sub.getIcon()!=null)
						sublist.put("icon",sub.getIcon());
					if(sub.getItemId()!=null)
						sublist.put("iten_id", sub.getItemId());
					if(sub.getName()!=null)
						sublist.put("name",sub.getName());
					if(sub.getSubItems()!=null)
						sublist.put("sub_items", new ArrayList<>());
					if(sub.getType()!=null)
						sublist.put("type", sub.getType());
					sublist.put("allow_delete", sub.isAllowDelete());
					subListArray.put(sublist);
				}
				
				json.put("sub_items", subListArray);
			}
			else
				json.put("sub_item",new JSONArray());
			json.put("allow_delete", genericEntityDto.isAllowDelete());
		}
		try {
			String response=postCloudApiData(url,json.toString());
			if(response!=null) {
				JSONObject responseJson= new JSONObject(response);
				respDto.setItemId(responseJson.getLong("item_id"));
				respDto.setMessage(responseJson.getString("message"));
				if(respDto.getItemId()!=null) {
					url=url+"?item_id="+respDto.getItemId().toString();
					String getResponse=getCloudApiData(url);
					if(getResponse!=null) {
						JSONObject getResponseJson=new JSONObject(getResponse);
						if(getResponseJson.has("data")) {
						//	JSONArray data =getResponseJson.getJSONArray("data");
							
							JSONObject singleData= getResponseJson.getJSONObject("data");
							if(singleData.has("allow_delete") && singleData.getInt("allow_delete")==1)
								getEntityDto.setAllowDelete(true);
							else
								getEntityDto.setAllowDelete(false);
							if(singleData.has("code") && !singleData.get("code").equals(null))
								getEntityDto.setCode(singleData.getString("code"));
							
							if (singleData.has("client_id"))
								getEntityDto.setClientId(singleData.getString("client_id"));
							
							if(singleData.has("type")&& !singleData.get("type").equals(null))
								getEntityDto.setType(singleData.getString("type"));
							
							if(singleData.has("icon") && !singleData.get("icon").equals(null))
								getEntityDto.setIcon(singleData.getLong("icon"));
							
							if(singleData.has("item_id"))
								getEntityDto.setItemId(singleData.getLong("item_id"));
							if(singleData.has("name"))
								getEntityDto.setName(singleData.getString("name"));
							
							if(singleData.has("sub_items") && singleData.length()>0) {
								JSONArray getSubItem=singleData.getJSONArray("sub_items");
								if(getSubItem.length()>0) {
									List<GenericEntityDto> listDto= new ArrayList<>();
									for(int i=0;i<getSubItem.length();i++) {
										GenericEntityDto addingSubItems= new GenericEntityDto();
										JSONObject sub= new JSONObject();
										sub= getSubItem.getJSONObject(i);
										if(sub.has("allow_delete") && sub.getInt("allow_delete")==1)
											addingSubItems.setAllowDelete(true);
										else
											addingSubItems.setAllowDelete(false);
										if(sub.has("client_id"))
											addingSubItems.setClientId(sub.getString("client_id"));
										
										if(sub.has("code") &&  !sub.get("code").equals(null))
											addingSubItems.setCode(sub.getString("code"));
										
										if(sub.has("color") && !sub.get("code").equals(null))
											addingSubItems.setColor(sub.getLong("color"));
										
										if(sub.has("icon") && !sub.get("code").equals(null))
											addingSubItems.setIcon(sub.getLong("icon"));
										
										if(sub.has("item_id"))
											addingSubItems.setItemId(sub.getLong("item_id"));
										if(sub.has("name"))
											addingSubItems.setName(sub.getString("name"));
										if(sub.has("sub_items"))
											addingSubItems.setSubItems(new ArrayList<>());
										if(sub.has("type"))
											addingSubItems.setType(sub.getString("type"));
										listDto.add(addingSubItems);
									}
									getEntityDto.setSubItems(listDto);
								}
								getEntityDto.setSubItems(new ArrayList<>());
							}
						}
					}else {
						throw new RuntimeException("Not Saved");
					}
				}
			}else {
				throw new RuntimeException("Data not created");
			}
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		return getEntityDto;
	}

	public String postCloudApiData(String url,String jsonToSent) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.postStringDataToServer(url,jsonToSent);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if(json.has("item_id")) {
					return response;
				}
		} else {
			return response;
		}
		return response;
	}
		return response;
	}
	
	public String getCloudApiData(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if(json.has("data")) {
					return response;
				}
		} else {
			return response;
		}
		return response;
	}
		return response;
	}
	
	public String deleteCloudApiData(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.deleteDataInServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if(json.has("message"))
					return response;
		} else {
			return response;
		}
		return response;
	}
		return response;
	}

	@Override
	public GenericEntityDto getGenericItemById(Long id) {
		
		GenericEntityDto getEntityDto=new GenericEntityDto();
		String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"?item_id=";
		url=url+id.toString();
		try {
		String getResponse=getCloudApiData(url);
		if(getResponse!=null) {
			JSONObject getResponseJson=new JSONObject(getResponse);
			if(getResponseJson.has("data")) {
			//	JSONArray data =getResponseJson.getJSONArray("data");
				
				JSONObject singleData= getResponseJson.getJSONObject("data");
				if(singleData.has("allow_delete") && singleData.getInt("allow_delete")==1)
					getEntityDto.setAllowDelete(true);
				else
					getEntityDto.setAllowDelete(false);
				if(singleData.has("code") && !singleData.get("code").equals(null))
					getEntityDto.setCode(singleData.getString("code"));
				
				if(singleData.has("type")&& !singleData.get("type").equals(null))
					getEntityDto.setType(singleData.getString("type"));
				
				if (singleData.has("client_id"))
					getEntityDto.setClientId(singleData.getString("client_id"));
				
				if(singleData.has("icon") && !singleData.get("icon").equals(null))
					getEntityDto.setIcon(singleData.getLong("icon"));
				
				if(singleData.has("item_id"))
					getEntityDto.setItemId(singleData.getLong("item_id"));
				if(singleData.has("name"))
					getEntityDto.setName(singleData.getString("name"));
				
				if(singleData.has("sub_items") && singleData.length()>0) {
					JSONArray getSubItem=singleData.getJSONArray("sub_items");
					if(getSubItem.length()>0) {
						List<GenericEntityDto> listDto= new ArrayList<>();
						
						for(int i=0;i<getSubItem.length();i++) {
							GenericEntityDto addingSubItems= new GenericEntityDto();
							JSONObject sub= new JSONObject();
							sub= getSubItem.getJSONObject(i);
							if(sub.has("allow_delete") && sub.getInt("allow_delete")==1)
								addingSubItems.setAllowDelete(true);
							else
								addingSubItems.setAllowDelete(false);
							if(sub.has("client_id"))
								addingSubItems.setClientId(sub.getString("client_id"));
							if(sub.has("code")  && !sub.get("code").equals(null))
								addingSubItems.setCode(sub.getString("code"));
							
							if(sub.has("color") && !sub.get("color").equals(null))
								addingSubItems.setColor(sub.getLong("color"));
							
							if(sub.has("icon") && !sub.get("icon").equals(null))
								addingSubItems.setIcon(sub.getLong("icon"));
							
							if(sub.has("item_id"))
								addingSubItems.setItemId(sub.getLong("item_id"));
							if(sub.has("name"))
								addingSubItems.setName(sub.getString("name"));
							if(sub.has("sub_items"))
								addingSubItems.setSubItems(new ArrayList<>());
							if(sub.has("type"))
								addingSubItems.setType(sub.getString("type"));
							listDto.add(addingSubItems);
							
						}
						getEntityDto.setSubItems(listDto);
					} else {
							getEntityDto.setSubItems(new ArrayList<>());
						}
				}
			}
		}else {
			throw new RuntimeException("Not Saved");
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return getEntityDto;
	}

	@Override
	public List<GenericEntityDto> getAllGenericItem() {
		String url = SOURCE_MANAGEMENT_CLOUD_URL + genericEntity;
		List<GenericEntityDto> allListItem = new ArrayList<>();
		try {
			String getResponse = getCloudApiData(url);
			if (getResponse != null) {
				JSONObject getResponseJson = new JSONObject(getResponse);
				if (getResponseJson.has("data")) {
					JSONArray data = getResponseJson.getJSONArray("data");
					for (int i = 0; i < data.length(); i++) {
						GenericEntityDto getEntityDto = new GenericEntityDto();
						JSONObject singleData = new JSONObject();
						singleData = data.getJSONObject(i);
						if (singleData.has("allow_delete") && singleData.getInt("allow_delete") == 1)
							getEntityDto.setAllowDelete(true);
						else
							getEntityDto.setAllowDelete(false);
						if (singleData.has("code") && !singleData.get("code").equals(null))
							getEntityDto.setCode(singleData.getString("code"));

						if (singleData.has("client_id"))
							getEntityDto.setClientId(singleData.getString("client_id"));

						if (singleData.has("type") && !singleData.get("type").equals(null))
							getEntityDto.setType(singleData.getString("type"));

						if (singleData.has("icon") && !singleData.get("icon").equals(null))
							getEntityDto.setIcon(singleData.getLong("icon"));

						if (singleData.has("item_id"))
							getEntityDto.setItemId(singleData.getLong("item_id"));
						if (singleData.has("name"))
							getEntityDto.setName(singleData.getString("name"));

						if (singleData.has("sub_items") && singleData.length() > 0) {
							JSONArray getSubItem = singleData.getJSONArray("sub_items");
							if (getSubItem.length() > 0) {
								List<GenericEntityDto> listDto = new ArrayList<>();

								for (int j = 0; j < getSubItem.length(); j++) {
									GenericEntityDto addingSubItems = new GenericEntityDto();
									JSONObject sub = new JSONObject();
									sub = getSubItem.getJSONObject(j);
									if (sub.has("allow_delete") && sub.getInt("allow_delete") == 1)
										addingSubItems.setAllowDelete(true);
									else
										addingSubItems.setAllowDelete(false);
									if (sub.has("client_id"))
										addingSubItems.setClientId(sub.getString("client_id"));
									if (sub.has("code") && !sub.get("code").equals(null))
										addingSubItems.setCode(sub.getString("code"));

									if (sub.has("color") && !sub.get("color").equals(null))
										addingSubItems.setColor(sub.getLong("color"));

									if (sub.has("icon") && !sub.get("icon").equals(null))
										addingSubItems.setIcon(sub.getLong("icon"));

									if (sub.has("item_id"))
										addingSubItems.setItemId(sub.getLong("item_id"));
									if (sub.has("name"))
										addingSubItems.setName(sub.getString("name"));
									if (sub.has("sub_items"))
										addingSubItems.setSubItems(new ArrayList<>());
									if (sub.has("type"))
										addingSubItems.setType(sub.getString("type"));
									listDto.add(addingSubItems);

								}
								getEntityDto.setSubItems(listDto);
							} else {
								getEntityDto.setSubItems(new ArrayList<>());
							}
						}
						allListItem.add(getEntityDto);
					}
				}
			} else {
				throw new RuntimeException("Not Saved");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return allListItem;
	}

	@Override
	public Boolean deleteGenericEntityByid(Long id) {
		String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"?item_id=";
		Boolean deleteResult=null;
		try {
		String getresponse=deleteCloudApiData(url);
		if(getresponse!=null) {
			JSONObject getResponseJson=new JSONObject(getresponse);
			if(getResponseJson.has("message") && getResponseJson.getString("message").equals("deleted")) {
				deleteResult=true;
				return deleteResult;
			}
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return deleteResult;
	}

	@Override
	public GenericEntityDto updateGenericEntity(GenericEntityDto genericEntityDto) {

		String url = SOURCE_MANAGEMENT_CLOUD_URL + genericEntity;
		GenericResponseDto respDto = new GenericResponseDto();
		GenericEntityDto getEntityDto = new GenericEntityDto();
		JSONObject json = new JSONObject();
		if (genericEntityDto != null) {
			if (genericEntityDto.getName() != null)
				json.put("name", genericEntityDto.getName());
			json.put("client_id", clientId);
			if (genericEntityDto.getParentId() != null)
				json.put("parent_id", genericEntityDto.getParentId());
			if (genericEntityDto.getType() != null)
				json.put("type", genericEntityDto.getType());
			if (genericEntityDto.getCode() != null) {
				json.put("code", genericEntityDto.getCode());
			}
			if (genericEntityDto.getCode() != null) {
				json.put("icon", genericEntityDto.getIcon());
			}
			if (genericEntityDto.getCode() != null) {
				json.put("color", genericEntityDto.getColor());
			}
			if (genericEntityDto.getCode() != null) {
				json.put("item_id", genericEntityDto.getItemId());
			}

			if (genericEntityDto.getSubItems() != null) {
				JSONArray subListArray = new JSONArray();
				for (GenericEntityDto sub : genericEntityDto.getSubItems()) {
					JSONObject sublist = new JSONObject();
					sublist.put("client_id", clientId);
					if (sub.getCode() != null)
						sublist.put("code", sub.getCode());
					if (sub.getColor() != null)
						sublist.put("color", sub.getColor());
					if (sub.getIcon() != null)
						sublist.put("icon", sub.getIcon());
					if (sub.getItemId() != null)
						sublist.put("item_id", sub.getItemId());
					if (sub.getName() != null)
						sublist.put("name", sub.getName());
					if (sub.getSubItems() != null)
						sublist.put("sub_items", new ArrayList<>());
					if (sub.getType() != null)
						sublist.put("type", sub.getType());
					sublist.put("allow_delete", sub.isAllowDelete());
					subListArray.put(sublist);
				}

				json.put("sub_items", subListArray);
			} else
				json.put("sub_item", new JSONArray());
			json.put("allow_delete", genericEntityDto.isAllowDelete());
		}
		try {
			String response = postCloudApiData(url, json.toString());
			if (response != null) {
				JSONObject responseJson = new JSONObject(response);
				if (responseJson.has("item_id")) {
					respDto.setItemId(responseJson.getLong("item_id"));
					respDto.setMessage(responseJson.getString("message"));
					if (respDto.getItemId() != null) {
						url = url + "?item_id=" + respDto.getItemId().toString();
						String getResponse = getCloudApiData(url);
						if (getResponse != null) {
							JSONObject getResponseJson = new JSONObject(getResponse);
							if (getResponseJson.has("data")) {
								JSONArray data = getResponseJson.getJSONArray("data");
								JSONObject singleData = data.getJSONObject(0);
								if (singleData.has("allow_delete") && singleData.getInt("allow_delete") == 1)
									getEntityDto.setAllowDelete(true);
								else
									getEntityDto.setAllowDelete(false);
								if (singleData.has("code") && !singleData.get("code").equals(null))
									getEntityDto.setCode(singleData.getString("code"));

								if (singleData.has("client_id"))
									getEntityDto.setClientId(singleData.getString("client_id"));

								if (singleData.has("type") && !singleData.get("type").equals(null))
									getEntityDto.setType(singleData.getString("type"));

								if (singleData.has("icon") && !singleData.get("icon").equals(null))
									getEntityDto.setIcon(singleData.getLong("icon"));

								if (singleData.has("item_id"))
									getEntityDto.setItemId(singleData.getLong("item_id"));
								if (singleData.has("name"))
									getEntityDto.setName(singleData.getString("name"));

								if (singleData.has("sub_items") && singleData.length() > 0) {
									JSONArray getSubItem = singleData.getJSONArray("sub_items");
									if (getSubItem.length() > 0) {
										List<GenericEntityDto> listDto = new ArrayList<>();
										for (int i = 0; i < getSubItem.length(); i++) {
											GenericEntityDto addingSubItems = new GenericEntityDto();
											JSONObject sub = new JSONObject();
											sub = getSubItem.getJSONObject(i);
											if (sub.has("allow_delete") && sub.getInt("allow_delete") == 1)
												addingSubItems.setAllowDelete(true);
											else
												addingSubItems.setAllowDelete(false);
											if (sub.has("client_id"))
												addingSubItems.setClientId(sub.getString("client_id"));

											if (sub.has("code") && !sub.get("code").equals(null))
												addingSubItems.setCode(sub.getString("code"));

											if (sub.has("color") && !sub.get("code").equals(null))
												addingSubItems.setColor(sub.getLong("color"));

											if (sub.has("icon") && !sub.get("code").equals(null))
												addingSubItems.setIcon(sub.getLong("icon"));

											if (sub.has("item_id"))
												addingSubItems.setItemId(sub.getLong("item_id"));
											if (sub.has("name"))
												addingSubItems.setName(sub.getString("name"));
											if (sub.has("sub_items"))
												addingSubItems.setSubItems(new ArrayList<>());
											if (sub.has("type"))
												addingSubItems.setType(sub.getString("type"));
											listDto.add(addingSubItems);
										}
										getEntityDto.setSubItems(listDto);
									}
									getEntityDto.setSubItems(new ArrayList<>());
								}
							}
						} else {
							throw new RuntimeException("Not Saved");
						}
					}
				} else {
					throw new RuntimeException("Data not created");
				}
			} else {
				throw new RuntimeException("Data not created");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return getEntityDto;

		// return null;
	}

	@Override
	public List<GenericEntityDto> getGenericItemByType(String type) {

		String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"?item_type=";
		url=url+type;
		List<GenericEntityDto> allListItem= new ArrayList<>();
		try {
			String getResponse = getCloudApiData(url);
			if (getResponse != null) {
				JSONObject getResponseJson = new JSONObject(getResponse);
				if (getResponseJson.has("data")) {
					JSONArray data = getResponseJson.getJSONArray("data");
					for (int i = 0; i < data.length(); i++) {
						GenericEntityDto getEntityDto = new GenericEntityDto();
						JSONObject singleData = new JSONObject();
						singleData = data.getJSONObject(i);
						if (singleData.has("allow_delete") && singleData.getInt("allow_delete") == 1)
							getEntityDto.setAllowDelete(true);
						else
							getEntityDto.setAllowDelete(false);
						if (singleData.has("code") && !singleData.get("code").equals(null))
							getEntityDto.setCode(singleData.getString("code"));

						if (singleData.has("client_id"))
							getEntityDto.setClientId(singleData.getString("client_id"));

						if (singleData.has("type") && !singleData.get("type").equals(null))
							getEntityDto.setType(singleData.getString("type"));

						if (singleData.has("icon") && !singleData.get("icon").equals(null))
							getEntityDto.setIcon(singleData.getLong("icon"));

						if (singleData.has("item_id"))
							getEntityDto.setItemId(singleData.getLong("item_id"));
						if (singleData.has("name"))
							getEntityDto.setName(singleData.getString("name"));

						if (singleData.has("sub_items") && singleData.length() > 0) {
							JSONArray getSubItem = singleData.getJSONArray("sub_items");
							if (getSubItem.length() > 0) {
								List<GenericEntityDto> listDto = new ArrayList<>();

								for (int j = 0; j < getSubItem.length(); j++) {
									GenericEntityDto addingSubItems = new GenericEntityDto();
									JSONObject sub = new JSONObject();
									sub = getSubItem.getJSONObject(j);
									if (sub.has("allow_delete") && sub.getInt("allow_delete") == 1)
										addingSubItems.setAllowDelete(true);
									else
										addingSubItems.setAllowDelete(false);
									if (sub.has("client_id"))
										addingSubItems.setClientId(sub.getString("client_id"));
									if (sub.has("code") && !sub.get("code").equals(null))
										addingSubItems.setCode(sub.getString("code"));

									if (sub.has("color") && !sub.get("color").equals(null))
										addingSubItems.setColor(sub.getLong("color"));

									if (sub.has("icon") && !sub.get("icon").equals(null))
										addingSubItems.setIcon(sub.getLong("icon"));

									if (sub.has("item_id"))
										addingSubItems.setItemId(sub.getLong("item_id"));
									if (sub.has("name"))
										addingSubItems.setName(sub.getString("name"));
									if (sub.has("sub_items"))
										addingSubItems.setSubItems(new ArrayList<>());
									if (sub.has("type"))
										addingSubItems.setType(sub.getString("type"));
									listDto.add(addingSubItems);

								}
								getEntityDto.setSubItems(listDto);
							} else {
								getEntityDto.setSubItems(new ArrayList<>());
							}
						}
						allListItem.add(getEntityDto);
					}
				}
			} else {
				throw new RuntimeException("Not Saved");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

		return allListItem;
	
	}
	
}
