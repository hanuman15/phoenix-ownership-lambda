package com.amazonaws.lambda.ownership.sourcecredibility.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author suresh
 *
 */
/*
 * @Entity
 * 
 * @Table(name = "sc_sources")
 */
public class Sources implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy = GenerationType.AUTO)
	 */
	private Long sourceId;

	/* @Column(name = "sourceName") */
	private String sourceName;

	/* @Column(name = "sourceUrl") */
	private String sourceUrl;

	/* @Column(name = "sourceDisplayName") */
	private String sourceDisplayName;
	/*
	 * @Column(name = "entityId")
	 */
	private String entityId;

	/* @Column(name = "sourceType") */
	private String sourceType;
	
	/* @Column(name = "category") */
	private String category;

	/*
	 * @CreationTimestamp
	 * 
	 * @Column(name = "sourceCreatedDate")
	 */
	private Date sourceCreatedDate;

	/*
	 * @ManyToMany(cascade = CascadeType.ALL)
	 * 
	 * @JoinTable(name = "sc_source_domain", joinColumns = { @JoinColumn(name =
	 * "source_id") }, inverseJoinColumns = {
	 * 
	 * @JoinColumn(name = "domain_id") })
	 */
	private List<SourceDomain> sourceDomain;

	/*
	 * @ManyToMany(cascade = CascadeType.ALL)
	 * 
	 * @JoinTable(name = "sc_source_industry", joinColumns = { @JoinColumn(name =
	 * "source_id") }, inverseJoinColumns = {
	 * 
	 * @JoinColumn(name = "industry_id") })
	 */
	private List<SourceIndustry> sourceIndustry;

	/*
	 * @ManyToMany(cascade = CascadeType.ALL)
	 * 
	 * @JoinTable(name = "sc_source_jurisdiction", joinColumns = {
	 * 
	 * @JoinColumn(name = "source_id") }, inverseJoinColumns = { @JoinColumn(name =
	 * "jurisdiction_id") })
	 */
	private List<SourceJurisdiction> sourceJurisdiction;

	/*
	 * @ManyToMany(cascade = CascadeType.ALL)
	 * 
	 * @JoinTable(name = "sc_source_media", joinColumns = { @JoinColumn(name =
	 * "source_id") }, inverseJoinColumns = {
	 * 
	 * @JoinColumn(name = "media_id") })
	 */
	private List<SourceMedia> sourceMedia;

	/*
	 * @ManyToMany(cascade = CascadeType.ALL)
	 * 
	 * @JoinTable(name = "sc_source_classification", joinColumns = {
	 * 
	 * @JoinColumn(name = "source_id") }, inverseJoinColumns = { @JoinColumn(name =
	 * "classification_id") })
	 */
	private List<Classifications> classifications;

	/*
	 * @OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy =
	 * "sources")
	 */
	private List<SourceCredibility> sourceCredibilityList;

	public Sources() {
	}

	public Sources(String sourceName, String sourceUrl, String sourceDisplayName, String entityId, String sourceType,String category) {
		super();
		this.sourceName = sourceName;
		this.sourceUrl = sourceUrl;
		this.sourceDisplayName = sourceDisplayName;
		this.entityId = entityId;
		this.sourceType = sourceType;
		this.category=category;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getSourceDisplayName() {
		return sourceDisplayName;
	}

	public void setSourceDisplayName(String sourceDisplayName) {
		this.sourceDisplayName = sourceDisplayName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public List<SourceDomain> getSourceDomain() {
		return sourceDomain;
	}

	public void setSourceDomain(List<SourceDomain> sourceDomain) {
		this.sourceDomain = sourceDomain;
	}

	public List<SourceIndustry> getSourceIndustry() {
		return sourceIndustry;
	}

	public void setSourceIndustry(List<SourceIndustry> sourceIndustry) {
		this.sourceIndustry = sourceIndustry;
	}

	public List<SourceJurisdiction> getSourceJurisdiction() {
		return sourceJurisdiction;
	}

	public void setSourceJurisdiction(List<SourceJurisdiction> sourceJurisdiction) {
		this.sourceJurisdiction = sourceJurisdiction;
	}

	public List<SourceCredibility> getSourceCredibilityList() {
		return sourceCredibilityList;
	}

	public void setSourceCredibilityList(List<SourceCredibility> sourceCredibilityList) {
		this.sourceCredibilityList = sourceCredibilityList;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public List<Classifications> getClassifications() {
		return classifications;
	}

	public void setClassifications(List<Classifications> classifications) {
		this.classifications = classifications;
	}

	public List<SourceMedia> getSourceMedia() {
		return sourceMedia;
	}

	public void setSourceMedia(List<SourceMedia> sourceMedia) {
		this.sourceMedia = sourceMedia;
	}

	public Date getSourceCreatedDate() {
		return sourceCreatedDate;
	}

	public void setSourceCreatedDate(Date sourceCreatedDate) {
		this.sourceCreatedDate = sourceCreatedDate;
	}

}
