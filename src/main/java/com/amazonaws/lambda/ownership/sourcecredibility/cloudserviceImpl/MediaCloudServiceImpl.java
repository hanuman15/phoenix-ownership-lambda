package com.amazonaws.lambda.ownership.sourcecredibility.cloudserviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.amazonaws.lambda.ownership.rest.ServiceCallHelper;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudservice.MediaCloudService;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourceMediaDto;

@Service
//@PropertySource(value = "classpath:serviceurls.properties")
public class MediaCloudServiceImpl implements MediaCloudService{
	
	@Autowired
	 Environment env;
	ResourceBundle resource = ResourceBundle.getBundle("application");
	//@Value("${source_management_cloud_url}")
	private String SOURCE_MANAGEMENT_CLOUD_URL=System.getenv("SOURCE_MANAGEMENT_CLOUD_URL");
	
	private static String genericEntity="listitems";
	
	
	@Override
	public List<SourceMediaDto> getSourceMedia(String type) {
		List<SourceMediaDto> mediaList = new ArrayList<>();
		String url = buildCloudUrl(type);
		JSONObject json = new JSONObject();
		try {
			String response = getCloudApiData(url);
			if (response != null) {
				json = new JSONObject(response);
			}
			if (json.has("data")) {
				JSONArray data = json.getJSONArray("data");

				for (int i = 0; i < data.length(); i++) {
					SourceMediaDto singleDto= new SourceMediaDto();
					JSONObject singleData= new JSONObject();		
					singleData=data.getJSONObject(i);
					if (singleData.has("item_id"))
						singleDto.setMediaId(singleData.getLong("item_id"));
					if (singleData.has("name"))
						singleDto.setMediaName(singleData.getString("name"));
					mediaList.add(singleDto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mediaList;
	}
	public String getCloudApiData(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if(json.has("data")) {
					return response;
				}
		} else {
			return response;
		}
		return response;
	}
		return response;
	}
	
	public String buildCloudUrl(String item_type) {
		String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"?item_type="+item_type;
		return url;
	}
	
	
}
