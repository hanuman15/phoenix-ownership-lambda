package com.amazonaws.lambda.ownership.demo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.ResponseEntity;

import com.amazonaws.lambda.ownership.advancesearch.controller.AdvanceSearchController;
import com.amazonaws.lambda.ownership.advancesearch.dto.HierarchyDto;
import com.amazonaws.lambda.ownership.advancesearch.dto.OwnershipDto;
import com.amazonaws.lambda.ownership.demo.config.WebConfig;
import com.amazonaws.lambda.ownership.response.Response;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class LambdaFunctionHandler  extends AbstractHandler<WebConfig> implements RequestHandler<OwnershipDto, String> {
	HttpServletRequest request;
	static AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
			LambdaFunctionHandler.class.getPackage().getName());

	public LambdaFunctionHandler() {
		ctx.getAutowireCapableBeanFactory().autowireBean(this);
	}
	@Autowired
	AdvanceSearchController adc;
	@Override
	public String handleRequest(OwnershipDto input, Context context) {

		context.getLogger().log("Input: " + input.getIdentifier());
		Response lambdaResponse = new Response();
		HierarchyDto jsonString= new HierarchyDto();
		jsonString.setUrl(input.getUrl());
		try {
			//String responseofGet =adc.testing(input.getId()); 
			context.getLogger().log("Input: ownership formation ");
			System.out.println("Path in DTO  "+ input.getPath());
			ResponseEntity<?> path = adc.ownershipStructureFormation(jsonString, input.getMaxSubsidiarielevels(),
					input.getLowRange(), input.getHighRange(), request, input.getIdentifier().toLowerCase(),
					input.getNoOfSubsidiaries(), input.getOrganisationName(), input.getJuridiction(),
					input.getIsSubsidiariesRequired(), input.getStartDate(), input.getEndDate(), input.getPath(),input.getSource());

			lambdaResponse.setResponseMessage(path.getBody().toString()+"  Message from ADVANCEsearc ");

		} catch (Exception e) {
			e.printStackTrace();
			lambdaResponse.setResponseMessage(e.getMessage());
		}
		context.getLogger().log("Response : " + lambdaResponse);
		return lambdaResponse.getResponseMessage();

	}

}
