package com.amazonaws.lambda.ownership.demo.config;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

//@EnableWebMvc
/*
 * @EnableAsync
 * 
 * @ComponentScan("com.amazonaws.lambda.ownership.advancesearch.serviceImpl")
 */
@Configuration
@ComponentScan("com.amazonaws.lambda.ownership")
public class WebConfig {
	
	/*
	 * @Bean public Environment getenv() { return new }
	 */
	

	@Bean(name = "taskExecutor")
	public TaskExecutor threadPoolTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(20);
		executor.setMaxPoolSize(20);
		executor.setThreadNamePrefix("default_task_executor_thread");
		executor.initialize();
		return executor;
	}

	@Bean("executor")
	public ThreadPoolExecutor getTreadPool() {
		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(30);
		return executor;
	}

}
