package com.amazonaws.lambda.ownership.advancesearch.serviceImpl;

import java.util.Comparator;

import org.json.JSONObject;

public class PercentageJSONComparator implements Comparator<JSONObject> {


	@Override
	public int compare(JSONObject o1, JSONObject o2) {
		Double a1=o1.getDouble("indirectPercentage");
		Double a2=o2.getDouble("indirectPercentage");
		return a2.compareTo(a1);

		/*if (o1.getDouble("indirectPercentage")>o2.getDouble("indirectPercentage"))
			return -1;
		else if(o1.getDouble("indirectPercentage")<o2.getDouble("indirectPercentage"))
			return 1;
		else
			return 0;*/
	}

}
