package com.amazonaws.lambda.ownership.advancesearch.service;

import java.util.List;

import com.amazonaws.lambda.ownership.advancesearch.dto.EntityTypeInput;
import com.amazonaws.lambda.ownership.advancesearch.dto.EntityTypeOutput;
import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface EntityLambdaService {
	
	@LambdaFunction(functionName="be-ner")
	List<EntityTypeOutput> getEntityType(EntityTypeInput input);

}
