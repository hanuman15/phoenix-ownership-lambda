package com.amazonaws.lambda.ownership.advancesearch.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.json.JSONException;

import com.amazonaws.lambda.ownership.advancesearch.dto.HierarchyDto;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourcesDto;

/**
 * 
 * @author Viswanath Reddy G
 *
 */
public interface AdvanceSearchService {

	
	String getHierarchyData(HierarchyDto hierarchyDto, Long userId, String apiKey) throws Exception;
	Boolean ownershipStructure(String identifier, HierarchyDto jsonString, Integer maxlevel, Integer lowRange,
			Integer highRange, String path, Integer noOfSubsidiaries, Integer shareholder_level,
			String organisationName, String juridiction, Long userId, Boolean isSubsidiariesRequired, String startDate, String endDate,boolean isRollBack, List<SourcesDto> sourcesDtos, String source) throws JSONException, Exception;

	String readCorporateStructure(String path) throws JSONException, Exception;
	String getMultisourceData(String query, String jurisdiction, String website, Long userId) throws Exception;
	String buildMultiSourceUrl(String identifier, String fields, String graph, Long userId, Integer level,
			String clientId) throws UnsupportedEncodingException;
	String getAnnualReturnsLink(String documentsLink) throws Exception;
	
	

}
