package com.amazonaws.lambda.ownership.advancesearch.dto;

public class EntityTypeOutput {
	
	private String entity;
	private String type;

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
