package com.amazonaws.lambda.ownership.advancesearch.dto;

public class ProfileResponseDto {
	
	private String news;
	
	private String watchlist;
	
	private String basic;

	public String getNews() {
		return news;
	}

	public void setNews(String news) {
		this.news = news;
	}

	public String getWatchlist() {
		return watchlist;
	}

	public void setWatchlist(String watchlist) {
		this.watchlist = watchlist;
	}

	public String getBasic() {
		return basic;
	}

	public void setBasic(String basic) {
		this.basic = basic;
	}

}
