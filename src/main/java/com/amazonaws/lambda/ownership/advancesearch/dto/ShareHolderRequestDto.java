package com.amazonaws.lambda.ownership.advancesearch.dto;

public class ShareHolderRequestDto {
	
	private String identifier;
	
	private String sourceReferenceID;
	
	private String from;
	
	private String referenceSource;
	
	private String sourceUrl;
	
	private String totalPercentage;
	
	private String vcardOrganizationName;
	
	private ShareholderLinks _links;

	private String shareHolders;
	
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getSourceReferenceID() {
		return sourceReferenceID;
	}

	public void setSourceReferenceID(String sourceReferenceID) {
		this.sourceReferenceID = sourceReferenceID;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getReferenceSource() {
		return referenceSource;
	}

	public void setReferenceSource(String referenceSource) {
		this.referenceSource = referenceSource;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getTotalPercentage() {
		return totalPercentage;
	}

	public void setTotalPercentage(String totalPercentage) {
		this.totalPercentage = totalPercentage;
	}

	public String getVcardOrganizationName() {
		return vcardOrganizationName;
	}

	public void setVcardOrganizationName(String vcardOrganizationName) {
		this.vcardOrganizationName = vcardOrganizationName;
	}

	public ShareholderLinks get_links() {
		return _links;
	}

	public void set_links(ShareholderLinks _links) {
		this._links = _links;
	}

	public String getShareHolders() {
		return shareHolders;
	}

	public void setShareHolders(String shareHolders) {
		this.shareHolders = shareHolders;
	}

}
