package com.amazonaws.lambda.ownership.advancesearch.controller;

import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/*
import element.bst.elementexploration.rest.extention.advancesearch.dto.HierarchyDto;
import element.bst.elementexploration.rest.extention.advancesearch.service.AdvanceSearchService;
import element.bst.elementexploration.rest.extention.sourcecredibility.cloudDto.SourcesPaginationDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.cloudservice.ClassificationCloudService;
import element.bst.elementexploration.rest.extention.sourcecredibility.cloudservice.SourceCloudService;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.ClassificationsDto;
import element.bst.elementexploration.rest.extention.sourcecredibility.dto.SourcesDto;*/

import com.amazonaws.lambda.ownership.advancesearch.dto.HierarchyDto;
import com.amazonaws.lambda.ownership.advancesearch.service.AdvanceSearchService;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudDto.SourcesPaginationDto;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudservice.ClassificationCloudService;
import com.amazonaws.lambda.ownership.sourcecredibility.cloudservice.SourceCloudService;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.ClassificationsDto;
import com.amazonaws.lambda.ownership.sourcecredibility.dto.SourcesDto;

/**
 * 
 * @author Viswanath Reddy G
 *
 */

@RestController
@Component
@RequestMapping("/api/advancesearch")
//@PropertySource(value = "classpath:application.properties")
public class AdvanceSearchController {


	
	@Autowired
	AdvanceSearchService advanceSearchService;
	String version = ResourceBundle.getBundle("application").getString("corporate_structure_location");
	//@Value("${corporate_structure_location}")
	private String CORPORATE_STRUCTURE_LOCATION ;

	@Autowired
	private SourceCloudService sourceCloudService;

	@Autowired
	private ClassificationCloudService classificationCloudService;

	@GetMapping(value = "test")
	public String testing(@RequestBody Long id) {
		String hell=sourceCloudService.getSourceById(1l).toString();
		return "hello World Property  " +" -  "+version+ " " + hell;
	}

	@PostMapping(value = "/ownershipStructure", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> ownershipStructure(@RequestBody HierarchyDto jsonString,

			@RequestParam(required = false) Integer maxSubsidiarielevels,

			@RequestParam(required = false) Integer lowRange, @RequestParam(required = false) Integer highRange,

			 HttpServletRequest request, @RequestParam String identifier,

			@RequestParam(required = false) Integer noOfSubsidiaries, @RequestParam String organisationName,

			@RequestParam String juridiction,

			@RequestParam(value = "isSubsidiariesRequired", required = false) Boolean isSubsidiariesRequired,

			@RequestParam(value = "startDate", required = false) String startDate,

			@RequestParam(value = "endDate", required = false) String endDate,

			@RequestParam(value = "source", required = false) String source) throws Exception {

		maxSubsidiarielevels = maxSubsidiarielevels != null ? maxSubsidiarielevels : 5;
		lowRange = lowRange != null ? lowRange : 10;
		highRange = highRange != null ? highRange : 100;
		noOfSubsidiaries = noOfSubsidiaries != null ? noOfSubsidiaries : 5;
		Long classificationId = null;
		System.out.println("Inside ownership Controller");
		List<ClassificationsDto> classificationsDtos = classificationCloudService.getClassifications("classification");

		for (ClassificationsDto classificationsDto : classificationsDtos) {
			if (classificationsDto.getClassifcationName().equalsIgnoreCase("GENERAL")) {
				classificationId = classificationsDto.getClassificationId();
			}
		}
		SourcesPaginationDto dto = sourceCloudService.getSources(0, 0, null, classificationId, null, null, null, null,
				false);

		List<SourcesDto> sourcesDtos = dto.getSourceList();

		String path = "";
		String requestId = UUID.randomUUID().toString();
		path = identifier + "-" + (maxSubsidiarielevels.toString()) + "-" + (lowRange.toString()) + "-"
				+ (highRange.toString()) + "-" + (noOfSubsidiaries.toString()) + "-" + requestId;
		System.out.println("Inside ownership Controller");
		System.out.println("Calling to ownership serviceimpl");
		advanceSearchService.ownershipStructure(identifier, jsonString, maxSubsidiarielevels, lowRange, highRange, path,
				noOfSubsidiaries, 30, organisationName, juridiction, 12345L, isSubsidiariesRequired, startDate, endDate,
				false, sourcesDtos, source);
		JSONObject json = new JSONObject();

		System.out.println("Calling is back from ownership serviceimpl");		
		json.put("path", path);
		return new ResponseEntity<>(json.toString(), HttpStatus.OK);
	}
	
	@PostMapping(value = "/ownershipStructureFormation", consumes = { "application/json; charset=UTF-8" }, produces = {
			"application/json; charset=UTF-8" })
	public ResponseEntity<?> ownershipStructureFormation(@RequestBody HierarchyDto jsonString,
			@RequestParam(required = false) Integer maxSubsidiarielevels,
			@RequestParam(required = false) Integer lowRange, @RequestParam(required = false) Integer highRange,
			HttpServletRequest request, @RequestParam String identifier,
			@RequestParam(required = false) Integer noOfSubsidiaries, @RequestParam String organisationName,
			@RequestParam String juridiction,
			@RequestParam(value = "isSubsidiariesRequired", required = false) Boolean isSubsidiariesRequired,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "path", required = false) String path,
			@RequestParam(value = "source", required = false) String source) throws Exception {
		System.out.println("Inside ownershipStructureFormation");
		maxSubsidiarielevels = maxSubsidiarielevels != null ? maxSubsidiarielevels : 5;
		lowRange = lowRange != null ? lowRange : 10;
		highRange = highRange != null ? highRange : 100;
		noOfSubsidiaries = noOfSubsidiaries != null ? noOfSubsidiaries : 5;
		Long classificationId = null;
		
		List<ClassificationsDto> classificationsDtos = classificationCloudService.getClassifications("classification");

		for (ClassificationsDto classificationsDto : classificationsDtos) {
			if (classificationsDto.getClassifcationName().equalsIgnoreCase("GENERAL")) {
				classificationId = classificationsDto.getClassificationId();
			}
		}
		SourcesPaginationDto dto = sourceCloudService.getSources(0, 0, null, classificationId, null, null, null, null,
				false);

		List<SourcesDto> sourcesDtos = dto.getSourceList();

	//	String path = "";
		String requestId = UUID.randomUUID().toString();
		/*
		 * path = identifier + "-" + (maxSubsidiarielevels.toString()) + "-" +
		 * (lowRange.toString()) + "-" + (highRange.toString()) + "-" +
		 * (noOfSubsidiaries.toString()) + "-" + requestId;
		 */
		System.out.println("Inside ownershipStructureFormation"+path);
		System.out.println("Calling to ownership serviceimpl");
		advanceSearchService.ownershipStructure(identifier, jsonString, maxSubsidiarielevels, lowRange, highRange, path,
				noOfSubsidiaries, 30, organisationName, juridiction, 12345L, isSubsidiariesRequired, startDate, endDate,
				false, sourcesDtos, source);
		JSONObject json = new JSONObject();

		System.out.println("Calling is back from ownership serviceimpl");
		json.put("path", path);
		return new ResponseEntity<>(json.toString(), HttpStatus.OK);
	}

	/*
	 * //@GetMapping(value = "/getCorporateStructure", produces = {
	 * "application/json; charset=UTF-8" }) public String
	 * getOwnershipPath(@RequestParam String path, @RequestParam("token") String
	 * token, HttpServletRequest request) throws Exception {
	 * 
	 * return (advanceSearchService.readCorporateStructure(path)); }
	 */

}
