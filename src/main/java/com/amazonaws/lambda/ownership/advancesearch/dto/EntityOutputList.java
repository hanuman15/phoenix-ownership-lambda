package com.amazonaws.lambda.ownership.advancesearch.dto;

import java.util.List;

public class EntityOutputList {
	
	private List<EntityTypeOutput> entityList;

	public List<EntityTypeOutput> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<EntityTypeOutput> entityList) {
		this.entityList = entityList;
	}
	
}
