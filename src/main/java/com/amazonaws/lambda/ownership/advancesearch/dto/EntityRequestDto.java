package com.amazonaws.lambda.ownership.advancesearch.dto;

import java.io.Serializable;
import java.util.List;

public class EntityRequestDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<EntityRequestDataDto> entityRequest;
	
	private List<OfficerShipRequestDto> officersRequest;


	public List<EntityRequestDataDto> getEntityRequest() {
		return entityRequest;
	}

	public void setEntityRequest(List<EntityRequestDataDto> entityRequest) {
		this.entityRequest = entityRequest;
	}

	public List<OfficerShipRequestDto> getOfficersRequest() {
		return officersRequest;
	}

	public void setOfficersRequest(List<OfficerShipRequestDto> officersRequest) {
		this.officersRequest = officersRequest;
	}

}
